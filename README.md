Emacs Powerpack README -- Currell Berry -- 03/31/2018
=================================================

Emacs Powerpack is an Emacs installer for Windows.  It takes care of creating a
start menu entry, creating a right click "open with" dropdown, add/remove programs 
in the registry, etc.  It also comes with a default configuration which enables
good interopability with msys2, which can be separately installed frim msys2.org.
The goal of emacs powerpack is to make it as easy as possible to set up a 
productive emacs-based unix-like development environment on a windows computer.

Download v0.92
-------------------------------------------------------------------

NOTE: file hosting for the project is now on sourceforge, at https://sourceforge.net/projects/emacs-powerpack .

[Download v0.92](https://sourceforge.net/projects/emacs-powerpack/files/Emacs_PowerPack_v0.92.exe/download)

What Emacs Powerpack Does
-------------------------------------------------------------------
Once Emacs Powerpack and Msys2 are installed, several things should be accomplished for you:

1. You can type "emacs" in the start menu to find and launch emacs.
2. Context menus should have an "open with emacs" option.
3. You have an easy choice of integrating emacs with windows cmd shell or msys2 shell
4. Features like M-x grep and M-x shell should work

Installation Directions
---------------------------------------------------------------------

1. (optional but recommended) First install msys2 from http://www.msys2.org/ to get a good unix-like shell installed on your system.

2. Then download emacs powerpack and run the installer.
    
Uninstallation
---------------------------------------------------------------------

Use Add/Remove Programs feature in Windows.


Notes
---------------------------------------------------------------------
In certain edge cases, you may have to restart windows before changes to PATH take effect.

Note that if "open with emacs" ever stops working it's probably because the .emacs.d/server/server file has gotten out of date (windows uses tcp to manage emacsclient connections, this is less robust than the method used on unix/linux.).  I have seen this occur when the computer crashes with emacs still running.  Delete the server file and try again.

Build Instructions
----------------------------------------------------------------------
- Check out project
- Download latest version of emacs for Windows as a zip file.  Make new folder named externals inside the project root and place the emacs zip there.
- Download nsis2 and install that
- Move the included nsisunz.dll into the Plugins folder of your nsis2 installation (we require this nsis plugin to extract the emacs zip at installation time)
- Right click on installer3.nsi in windows explorer, and select "Compile NSIS Script"
- NSIS should run and produce you a working Emacs_Powerpack.exe isntaller of your own
